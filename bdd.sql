-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 21 mai 2021 à 10:59
-- Version du serveur :  10.4.16-MariaDB
-- Version de PHP : 7.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `rs2`
--

-- --------------------------------------------------------

--
-- Structure de la table `conversation`
--

CREATE TABLE `conversation` (
  `id` int(11) NOT NULL,
  `nom_conversation` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `conversation`
--

INSERT INTO `conversation` (`id`, `nom_conversation`) VALUES
(1, 'conv1'),
(2, 'conv2'),
(33, 'Conv3'),
(34, 'conv5'),
(35, 'conv6'),
(36, 'Conv8');

-- --------------------------------------------------------

--
-- Structure de la table `follow`
--

CREATE TABLE `follow` (
  `id` int(11) NOT NULL,
  `id_follower` int(11) NOT NULL,
  `id_following` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `follow`
--

INSERT INTO `follow` (`id`, `id_follower`, `id_following`) VALUES
(15, 35, 40),
(16, 37, 36),
(17, 37, 35),
(22, 35, 37),
(24, 36, 37),
(25, 36, 39),
(26, 36, 41),
(27, 35, 39),
(30, 35, 38);

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `type` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `likes`
--

INSERT INTO `likes` (`id`, `id_post`, `id_user`, `type`) VALUES
(43, 150, 35, 'D'),
(44, 154, 35, 'L'),
(45, 156, 35, 'L'),
(46, 163, 37, 'D'),
(47, 161, 37, 'L'),
(48, 154, 37, 'D'),
(49, 164, 36, 'L'),
(50, 165, 35, 'D'),
(51, 166, 35, 'L'),
(52, 171, 35, 'L'),
(53, 169, 23, 'L'),
(54, 170, 23, 'L'),
(55, 171, 23, 'D'),
(56, 170, 35, 'L');

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `content` text DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`id`, `content`, `created_at`, `user_id`) VALUES
(154, 'Hello', '2021-05-13 13:07:36', 35),
(161, 'dsdqs', '2021-05-14 19:08:38', 35),
(162, 'dqsdq', '2021-05-14 19:08:38', 35),
(163, 'qsdqsdq', '2021-05-14 19:08:39', 35),
(165, 'Heyy\n', '2021-05-15 22:35:35', 36),
(166, 'ssss', '2021-05-18 13:28:30', 35),
(167, 'fefzefzefe', '2021-05-18 14:54:39', 35),
(168, 'feezfez', '2021-05-18 14:54:48', 35);

-- --------------------------------------------------------

--
-- Structure de la table `messages_conv`
--

CREATE TABLE `messages_conv` (
  `id` int(11) NOT NULL,
  `content` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_conv` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `messages_conv`
--

INSERT INTO `messages_conv` (`id`, `content`, `created_at`, `id_user`, `id_conv`) VALUES
(1, 'allo', '2021-05-06 14:47:37', 35, 1),
(2, 'hello', '2021-05-06 14:47:37', 36, 1),
(3, 'text', '2021-05-06 14:47:37', 36, 2),
(4, 'alala', '2021-05-06 14:47:37', 35, 2),
(6, 'Heyy', '2021-05-13 19:30:57', 35, 1),
(7, 'Tu veux me quitter c est mort !', '2021-05-13 20:10:02', 35, 1),
(8, 'Allo', '2021-05-13 20:17:03', 35, 2),
(9, 'Je veux plus de tes efforts coco', '2021-05-13 20:17:54', 36, 1),
(10, 'sfddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', '2021-05-13 20:18:02', 36, 1),
(11, 'dsqdkmkqsds', '2021-05-14 19:09:27', 35, 36),
(13, 'hello', '2021-05-19 13:08:30', 35, 36);

-- --------------------------------------------------------

--
-- Structure de la table `relation_userconv`
--

CREATE TABLE `relation_userconv` (
  `id_user` int(11) NOT NULL,
  `id_conv` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `relation_userconv`
--

INSERT INTO `relation_userconv` (`id_user`, `id_conv`) VALUES
(23, 1),
(23, 2),
(35, 1),
(35, 2),
(35, 33),
(35, 34),
(35, 35),
(35, 36),
(36, 1),
(36, 2),
(36, 36),
(38, 1),
(39, 1),
(39, 36);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `role` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`iduser`, `name`, `firstname`, `birthdate`, `email`, `password`, `photo`, `role`) VALUES
(23, 'Vegeta', 'vegeta', '2021-04-13', 'vegeta', 'azerty', '/photo/c10fc5deb2e824b0d36c109071b8a41e.jpg', 'user'),
(35, 'Goku', 'san', '2021-04-07', 'goku', 'goku', '/photo/unnamed.jpg', 'admin'),
(36, 'beerus', 'samama', '2021-04-16', 'beerus', 'beerus', '/photo/Beerus_Battle_of_Gods.jpg', 'user'),
(38, 'hit', 'hit', '2021-04-20', 'hit', 'hit', '/photo/zccfmdop84iz.jpg', 'user'),
(39, 'krilin', 'krilin', '2021-04-13', 'krillin', 'krillin', '/photo/png-transparent-krillin-goku-gohan-master-roshi-trunks-goku-child-dragon-hand-thumbnail.jpg', 'user');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_user` (`user_id`);

--
-- Index pour la table `messages_conv`
--
ALTER TABLE `messages_conv`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`),
  ADD KEY `id_conv` (`id_conv`);

--
-- Index pour la table `relation_userconv`
--
ALTER TABLE `relation_userconv`
  ADD PRIMARY KEY (`id_user`,`id_conv`),
  ADD KEY `id_conv` (`id_conv`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`iduser`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `conversation`
--
ALTER TABLE `conversation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT pour la table `follow`
--
ALTER TABLE `follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=172;

--
-- AUTO_INCREMENT pour la table `messages_conv`
--
ALTER TABLE `messages_conv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `messages`
--
ALTER TABLE `messages`
  ADD CONSTRAINT `fk_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`iduser`);

--
-- Contraintes pour la table `messages_conv`
--
ALTER TABLE `messages_conv`
  ADD CONSTRAINT `messages_conv_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`iduser`),
  ADD CONSTRAINT `messages_conv_ibfk_2` FOREIGN KEY (`id_conv`) REFERENCES `conversation` (`id`);

--
-- Contraintes pour la table `relation_userconv`
--
ALTER TABLE `relation_userconv`
  ADD CONSTRAINT `relation_userconv_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`iduser`),
  ADD CONSTRAINT `relation_userconv_ibfk_2` FOREIGN KEY (`id_conv`) REFERENCES `conversation` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
