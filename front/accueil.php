<?php

session_start();
require_once('../back/fonction/db_connect.php');
require_once('../back/fonction/resultArray.php');
require_once('../back/fonction/checkUserConnect.php');

// Requête user + content
$req = "SELECT * FROM `messages` JOIN `user` on user.iduser = messages.user_id ORDER BY created_at DESC";
$res = $db->query($req);
$result = mysqli_fetch_all($res, MYSQLI_ASSOC);

// Requête photo des users
$reqphoto = "SELECT photo FROM user WHERE iduser = '{$_SESSION['user']['id']}' ";
$resphoto = $db->query($reqphoto);
$resultphoto = mysqli_fetch_assoc($resphoto);


?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/accueil.css">
    <script src="https://kit.fontawesome.com/30eaac9e43.js" crossorigin="anonymous"></script>
    <title>Document</title>
</head>

<body>

    <header>

        <div class="menu">

            <div class="logo">
                <h2>CHAT<span style="color: whitesmoke;">BOX</span></h2>
            </div>

        </div>

    </header>

    <section>


        <div class="divinfo">

            <div class="userlogin_photo">
                <div class="userphoto">

                    <img class="photo_profil" src="..<?= $resultphoto['photo']; ?>">

                </div>
                <br>
                <div class="userlogin">
                    <p class="p_userlogin"><?php echo ($_SESSION['user']['email']); ?> </p>
                </div>


            </div>
            <div class="div_a">

                <a class="a_update" href="./update.php"> Modifier profil</a>
                <a class="a_password" href="./update_password.html">Changer de mot de passe</a>
                <a class="a_conversation" href="./conversation.html">Conversations</a>
                <!-- <a class="a_admin" href="./conversation.html">Admin</a> -->
            </div>

            <div class="logout">
                <a class="logout" href="../back/deconnexion.php">Deconnexion</a>
            </div>

        </div>


        <div class="chat">

            <div class="divform">

                <form class="insert_post" method="POST">
                    <textarea id="input_text" class="input_text" placeholder=" Tapez votre texte ici ..." name="content"></textarea>
                    <input class="input_submit" type="submit" value="Envoyer">
                </form>
            </div>



            <div class="messages">
                <div class="zone_de_texte">


                    <?php foreach ($result as $val) { ?>
                        <table class="post">
                            <tr>
                                <td class="tdlogo">

                                    <img class="imgpost" src="..<?= $val['photo']; ?>">
                                    <h3 class="h3PostUsername"><?= $val['email']; ?></h3>

                                </td>
                            </tr>

                            <tr class="trContentPost">
                                <td class="tdContentPost"><?= $val['content']; ?></td>

                            </tr>
                            <tr class="trLikeDislike">
                                <td class="tdLikeDislike">
                                    <div>
                                        <a href="../back/like.php?type=like&postid=<?= $val['id']; ?>">
                                            <i style="color:greenyellow; font-size: 1.2em;" class="fas fa-thumbs-up">
                                                <?php
                                                $reqNumberLike = $db->query("SELECT * FROM likes WHERE id_post={$val['id']} AND type='L'");
                                                $numberLike =  resultAsArray($reqNumberLike);
                                                print_r(count($numberLike));
                                                ?>
                                            </i>
                                        </a>

                                    </div>

                                    <div>
                                        <a href="../back/like.php?type=dislike&postid=<?= $val['id']; ?>">
                                            <i style="color:red; font-size: 1.2em;" class="fas fa-thumbs-down">
                                                <?php
                                                $reqNumberLike = $db->query("SELECT * FROM likes WHERE id_post={$val['id']} AND type='D'");
                                                $numberLike =  resultAsArray($reqNumberLike);
                                                print_r(count($numberLike));
                                                ?>

                                            </i>
                                        </a>

                                    </div>
                                </td>

                            </tr>
                            <tr>

                                <?php
                                if ($val['user_id'] == $_SESSION['user']['id'] || $_SESSION['user']['role'] == 'admin') { ?>
                                    <td class="tdDeletePost">
                                        <a class="aDeletePost" href="../back/deletePost.php?idpost=<?= $val['id']; ?>">Supprimer</a>
                                    </td>
                                <?php } ?>
                            </tr>

                        </table>
                        <br>
                    <?php } ?>

                </div>
            </div>




        </div>

        <div class="divfriend">
            <br>
            <h1 style="color:white">Liste des utilisateurs</h1>
            <br>
            <table id="tableUser"></table>
            <br>
            <h1 style="color:white">Abonnement</h1>
            <br>
            <table id="tableFriend"></table>

        </div>
        <input type=hidden id=loginUser value=<?php echo $_SESSION['user']['email']; ?>>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="./js/accueil.js"></script>


</body>

</html>