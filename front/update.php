<?php

session_start();
require_once('../back/fonction/db_connect.php');

if (!isset($_SESSION['user'])) {
    header('location: ../index.html');
}

// Requête photo des users
$reqphoto = "SELECT photo FROM user WHERE iduser = '{$_SESSION['user']['id']}' ";
$resphoto = $db->query($reqphoto);
$resultphoto = mysqli_fetch_assoc($resphoto);

if (!empty($_POST['name'])) {
    $name = $_POST['name'];
    $req = "UPDATE user SET name = '$name' WHERE iduser = {$_SESSION['user']['id']}";
    $result = $db->query($req);

    if ($result) {

        $_SESSION['user']['name'] = $name;
    }
}

if (!empty($_POST['firstname'])) {
    $firstname = $_POST['firstname'];
    $req = "UPDATE user SET firstname = '$firstname' WHERE iduser = {$_SESSION['user']['id']}";
    $result = $db->query($req);

    if ($result) {

        $_SESSION['user']['firstname'] = $firstname;
    }
}

if (!empty($_POST['email'])) {
    $email = $_POST['email'];
    $req = "UPDATE user SET email = '$email' WHERE iduser = {$_SESSION['user']['id']}";
    $result = $db->query($req);
    if ($result) {

        $_SESSION['user']['email'] = $email;
    }
}

if (!empty($_POST['birthdate'])) {
    $birthdate = $_POST['birthdate'];
    $req = "UPDATE user SET birthdate = '$birthdate' WHERE iduser = {$_SESSION['user']['id']}";
    $result = $db->query($req);
    if ($result) {

        $_SESSION['user']['birthdate'] = $birthdate;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/update.css">
    <title>Update Password</title>
</head>

<body>

    <header>
        <div class="menu">

            <div class="logo">
                <h2>CHAT<span style="color: white;">BOX</span></h2>
            </div>

            <div class="info_user">
                <a class="a_update" href="./accueil.php"> Accueil</a>
                <a class="a_password" href="./update_password.php">Changer de mot de passe</a>

            </div>
        </div>

    </header>
    <section>




        <div class="all_form">

            <div class="bloc_photo">

                <div class="photo">
                    <img src="..<?= $resultphoto['photo']; ?>">
                </div>
                <div class="login">
                    <p class="p_userlogin"><?php echo ($_SESSION['user']['email']); ?> </p>
                </div>
                <div class="update_photo">

                    <form class="upload_form" enctype="multipart/form-data" action="../back/upload.php" method="post">
                        <input class="input_update_photo" name="userfile" type="file" />
                        <input class="input_update_photo" type="submit" value="Modifier photo de profil" />
                    </form>

                </div>
            </div>
            <br>
            <form method="POST">

                <label>Nom :</label>
                <input type="text" name="name" placeholder="   <?= $_SESSION['user']['name'] ?>">

                <br>
                <label>Prénom :</label>
                <input type="text" name="firstname" placeholder="   <?= $_SESSION['user']['firstname'] ?>">

                <br>
                <label>Login :</label>
                <input type="text" name="email" placeholder="   <?= $_SESSION['user']['email'] ?>">

                <br>
                <label>Date de naissance :</label>
                <input type="date" name="birthdate" placeholder="   <?= $_SESSION['user']['birthdate'] ?>">
                <br>
                <input class="input_update" type="submit" value="Modifier">

            </form>

        </div>

    </section>
</body>

</html>