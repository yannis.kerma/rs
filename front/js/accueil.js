const loginUser = $('#loginUser').val();

$('.input_submit').click((e) => {
    e.preventDefault();

    $.ajax({
        url: '../back/fluxAccueil.php',
        type: "POST",
        data: {
            message: $("#input_text").val(),
            choice: 'post'
        },
        dataType: "json",
        success: (res, status) => {
            document.location.reload();
        }

    })

})

$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'infoUser'
    },
    dataType: "json",
    success: function (res) {
        let html = ""
        let role = ""
        if (res['success'] == true) {
            res.infoUser.forEach(element => {
                role += element.role
            });
            if (role == "admin") {
                html += "<a class='a_admin' href='./admin.html'>Admin</a>"
                $('.div_a').append(html)
            }
        }

    }

})

$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'listUser'
    },
    dataType: "json",
    success: function (res) {

        if (res['success'] == true) {
            let user = ""
            res.userList.forEach(elm => {
                if (elm.email == loginUser) {
                    console.log('utilisateur proprio')
                } else {

                    user += "<tr> <td>" + elm.email + "</td> <td> <a href='../back/follow.php?idFollowing=" + elm.iduser + "'> Suivre </a> </td> </tr>"
                }
            });
            $('#tableUser').append(user)

        }

    }

})

$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'listFriend'
    },
    dataType: "json",
    success: function (res) {
        console.log('test')
        if (res['success'] == true) {
            let user = ""
            res.friendList.forEach(elm => {
                if (elm.email == loginUser) {
                    // console.log('utilisateur proprio')
                } else {

                    user += "<tr> <td>" + elm.email + "</td> <td> <a href='../back/unfollow.php?iduser=" + elm.iduser + "'> Se désabonner </a> </td> </tr>"
                }
            });
            $('#tableFriend').append(user)

        }

    }

})

$('#valid').click((e) => {
    e.preventDefault();
    const password = $('#password').val()
    const newPassword = $('#newpassword').val()
    console.log(newPassword)

    $.ajax({
        url: '../back/fluxAccueil.php',
        type: "POST",
        data: {
            choice: 'updatePassword',
            password: password,
            newPassword: newPassword
        },
        dataType: "json",
        success: function (res) {
            if (res["success"] == true) {
                console.log('mot de passe changé')
                let html = ""
                html += "<p> Nouveau mot de passe enregistré !</p>"
                $('#true').append(html)

            } else if (res["erreur"] == "wrongPassword") {
                console.log('faux password')
                let html = ""
                html += "<p> Mot de passe incorrect </p>"
                $('#erreur').append(html)

            } else if (res["erreur"] == "emptyChamp") {
                console.log('champ vide')
                let html = ""
                html += "<p> Veuillez remplir tout les champs </p>"
                $('#erreur').append(html)
            }
        }

    })

})