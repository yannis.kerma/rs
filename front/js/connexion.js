$('.submit').click((e) => {
    e.preventDefault();

    $.ajax({
        url: './back/connexion.php',
        type: "POST",
        data: {
            email: $("#email").val(),
            password: $("#password").val(),
        },
        dataType: "json",
        success: (res, status) => {
            if (res['success']) {
                window.location.href = '/rs/front/accueil.php';
            } else {
                $('#erreur').text("Utilisateur introuvable !");
            }
        }

    })

})

$('#erreur').css('color', 'red');