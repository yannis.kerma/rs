$('.submit').click((e) => {
    e.preventDefault();

    const name = $("#name").val();
    const firstname = $("#firstname").val();
    const birthdate = $("#birthdate").val();
    const email = $("#email").val();
    const password = $("#password").val();

    $.ajax({
        url: '../back/requete.php',
        type: "POST",
        data: {
            firstname: firstname,
            name: name,
            birthdate: birthdate,
            email: email,
            password: password,
        },
        dataType: "json",
        success: (res, status) => {
            if (res['success']) {
                window.location.href = '/rs';
                alert('utilisateur enregistré !')

            } else {
                console.log('relou')
            }
        }
    })

})