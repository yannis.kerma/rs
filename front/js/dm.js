let getUrlParam = new URLSearchParams(window.location.search)
let idconv = getUrlParam.get('idconv')

$.ajax({
    url: '../back/conversation_list.php',
    type: "POST",
    data: {
        choice: 'checkUser',
    },
    dataType: 'json',
    success: function(res) {

        if (res['success'] == false) {
            window.location.replace("http://localhost/rs/")

        }

    }
})

$.ajax({
    url: '../back/conversation_list.php',
    type: "POST",
    data: {
        choice: 'adduserlist',
        idconv: idconv

    },
    dataType: 'json',
    success: function(res) {

        if (res['success'] == true) {
            let user = ""
            res.user.forEach(elm => {
                user += "<option value='" + elm.iduser + "'>" + elm.email + "</option>";
            })
            $('select').append(user);
        }

    }
})

$.ajax({
    url: '../back/conversation_list.php',
    type: "POST",
    data: {
        choice: 'userlist',
        idconv: idconv

    },
    dataType: 'json',
    success: function(res) {

        if (res['success'] == true) {
            let user = ""
            res.user.forEach(elm => {
                user += "<tr> <td id='tdPhoto'> <img src='.." + elm.photo + "'> </td> <td id='tdUsername'>" + elm.email + "</td> </tr>";
            })
            $('table').append(user);
        }

    }
})


$.ajax({
    url: '../back/conversation_list.php',
    type: "POST",
    data: {
        choice: 'dm_message',
        idconv: idconv
    },
    dataType: 'json',
    success: function(res) {
        if (res["success"] == true) {

            const idsession = res.idsession

            let message = ''
            res.messagelist.forEach(element => {
                if (element.id_user == idsession) {
                    message += "<div class='postUserMessage'><p class='p_user'>" + element.content + "</p><p class='nameUser'>" + element.email + "</p></div><br>"
                } else {

                    message += "<div class='postMessage'><p class'p_otheruser'>" + element.content + "</p><p class='name'>" + element.email + "</p> </div> <br>"
                }
            });
            $('.message').append(message)
        } else if (res["success"] == false) {
            console.log('erreur')
        }

    }
})

$('#envoitext').click((e) => {
    // e.preventDefault();
    const textmessage = $('#textmessage').val();

    $.ajax({
        url: '../back/conversation_list.php',
        type: "POST",
        data: {
            choice: 'envoitext',
            idconv: idconv,
            textmessage: textmessage


        },
        dataType: 'json',
        success: function(res) {
            if (res['success'] == false) {
                let erreur = "<p> Erreur d'envoi </p>"
                $('diverreur').append(erreur);
            }
        }
    })
})

$('#adduser').click((e) => {
    // e.preventDefault();
    let user = $('#user').val()
        // console.log(user)

    $.ajax({
        url: '../back/conversation_list.php',
        type: "POST",
        data: {
            choice: 'adduser',
            idconv: idconv,
            userid: user,


        },
        dataType: 'json',
        success: function(res) {
            if (res['success'] == true) {
                console.log('utilisateur ajouté')
            }
        }
    })
})