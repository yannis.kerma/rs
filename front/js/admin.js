console.log('admin')


function deleteUser(iduser) {
    $.ajax({
        url: '../back/fluxAccueil.php',
        type: 'POST',
        data: {
            choice: 'deleteUser',
            id: iduser
        },
        dataType: 'json',
        success: (res, status) => {
            if (res.success) {
                window.location.replace("http://localhost/rs/front/admin.html")

            }
        }
    });
}

function deleteConv(idconv) {
    console.log(idconv)
    $.ajax({
        url: '../back/fluxAccueil.php',
        type: 'POST',
        data: {
            choice: 'deleteConv',
            id: idconv
        },
        dataType: 'json',
        success: (res, status) => {
            if (res.success) {
                window.location.replace("http://localhost/rs/front/admin.html")

            }
        }
    });
}

$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'listConvAdmin',
    },
    dataType: 'json',
    success: function (res) {
        let html = ""
        res.conv.forEach(elm => {
            html += "<tr class='tr_conv'>" + "<td>" + elm.nom_conversation + "</td>" + "<td>" + "<button onclick='deleteConv(" + elm.id + ")'>Supprimer</button>" + "</td>" + "</tr>"
        })
        $('.conversation').append(html)
    }
})


$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'infoUser'
    },
    dataType: "json",
    success: function (res) {
        let html = ""
        let role = ""
        if (res['success'] == true) {
            res.infoUser.forEach(element => {
                role += element.role
            });
            if (role == "admin") {
                // console.log('okk')
            } else {
                window.location.replace("http://localhost/rs/back/deconnexion.php")

            }
        }

    }

})


$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'count'
    },
    dataType: "json",
    success: function (res) {
        let countU = ""
        let countP = ""
        let countC = ""
        if (res['success'] == true) {
            countU += res.countUser
            countP += res.countPost
            countC += res.countConv

        }
        $(".numberUser").append(countU)
        $(".numberPost").append(countP)
        $(".numberConv").append(countC)
    }

})

$.ajax({
    url: '../back/fluxAccueil.php',
    type: "POST",
    data: {
        choice: 'users'
    },
    dataType: "json",
    success: function (res) {
        let html = "";
        res.userlist.forEach(elm => {
            // console.log(elm.iduser)
            html += "<tr class='user'> <td>" + "<img src='.." + elm.photo + "'> </td>" + "<td> <a>" + elm.email + "</a> </td>" + "<td> <button onclick='deleteUser(" + elm.iduser + ")'>Supprimer </button>" + "</td></tr>";
        });
        $('.containUser').append(html);


    }
})