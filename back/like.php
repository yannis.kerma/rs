<?php
session_start();
require_once('./fonction/db_connect.php');
require_once('./fonction/checkUserConnect.php');
require_once('../back/fonction/resultArray.php');



if (!empty($_GET['postid']) && !empty($_GET['type'])) {
    $postId = $_GET['postid'];
    $userId = $_SESSION['user']['id'];

    $reqCheckLike = $db->query("SELECT * FROM likes WHERE id_post=$postId AND id_user=$userId AND type='L'");
    $reqCheckDislike = $db->query("SELECT * FROM likes WHERE id_post=$postId AND id_user=$userId AND type='D'");
    $checkLike =  resultAsArray($reqCheckLike);
    // print_r($checkLike);
    $checkDislike = resultAsArray($reqCheckDislike);

    if ($_GET['type'] == 'like') {

        if (count($checkLike) == 1) {
            // echo "like déja présent";
            $DeleteLike = $db->query("DELETE FROM likes WHERE id_post=$postId AND id_user=$userId");
            header('location: ../front/accueil.php');
        } elseif (count($checkDislike) == 1) {
            $dislike = $db->query("UPDATE likes SET type='L' WHERE id_post=$postId AND id_user=$userId ");
            // echo "like update en L";
            header('location: ../front/accueil.php');
        } elseif (count($checkLike) == 0) {
            $insertLike = $db->query("INSERT INTO likes (id_post, id_user,type) VALUES ($postId, $userId,'L')");
            // echo "like ajouté";
            header('location: ../front/accueil.php');
        }
        header('location: ../front/accueil.php');
    }


    if ($_GET['type'] == 'dislike') {
        if (count($checkDislike) == 1) {
            // echo "dislike déja présent";
            $DeleteLike = $db->query("DELETE FROM likes WHERE id_post=$postId AND id_user=$userId");
            header('location: ../front/accueil.php');
        } elseif (count($checkLike) == 1) {
            $dislike = $db->query("UPDATE likes SET type='D' WHERE id_post=$postId AND id_user=$userId ");
            header('location: ../front/accueil.php');

            // echo "like update en D";
        } elseif (count($checkDislike) == 0) {
            $insertLike = $db->query("INSERT INTO likes (id_post, id_user,type) VALUES ($postId, $userId,'D')");
            // echo "dislike ajouté";
            header('location: ../front/accueil.php');
        }
    }
}
