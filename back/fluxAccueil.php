<?php
session_start();
require_once('./fonction/db_connect.php');
require_once('../back/fonction/resultArray.php');



switch ($_POST['choice']) {

    case 'post':


        if (!isset($_SESSION['user'])) { //1. Vérification que l'utilisateur est connecté
            header('location: ../index.html');
        }
        if (!empty($_POST['message'])) { //2. Récupérer contenu message form
            $message = $_POST['message'];
            $userid = $_SESSION['user']['id'];

            $req = "INSERT INTO `messages` ( content, created_at, user_id) VALUES ('$message', NOW(), '$userid')"; //3. Préparation requete pour insertion
            $db->query($req);

            echo json_encode(['success' => true]); //4. Confirmation
        }
        break;


    case 'listUser':

        $myId = $_SESSION['user']['id'];
        $reqUserList = $db->query("SELECT * from user WHERE iduser NOT IN (SELECT DISTINCT follow.id_following FROM follow
        WHERE id_follower = $myId)");
        $resUserList = resultAsArray($reqUserList);

        if ($resUserList) {
            echo json_encode(['success' => true, 'userList' => $resUserList]);
        }


        break;

    case 'listFriend':

        $myId = $_SESSION['user']['id'];
        $reqFriendList = $db->query("SELECT * from user WHERE iduser IN (SELECT DISTINCT follow.id_following FROM follow
        WHERE id_follower = $myId)");
        $resFriendList = resultAsArray($reqFriendList);

        if ($resFriendList) {
            echo json_encode(['success' => true, 'friendList' => $resFriendList]);
        }


        break;


    case 'updatePassword':

        if (!empty($_POST['password']) && !empty($_POST['newPassword'])) {

            $myid = $_SESSION['user']['id'];
            $password = $_POST['password'];
            $newpassword = $_POST['newPassword'];

            $reqVerif = $db->query("SELECT * FROM user WHERE iduser = $myid AND password = '$password' ");
            $result = mysqli_fetch_all($reqVerif, MYSQLI_ASSOC);

            if ($result) {
                $changePassword = $db->query("UPDATE user SET password = '$newpassword' WHERE iduser = $myid");
                echo json_encode(["success" => true]);
            } else {
                echo json_encode(["erreur" => "wrongPassword"]);
            }
        } else {
            echo json_encode(["erreur" => "emptyChamp"]);
        }



        break;


    case 'infoUser':

        $myId = $_SESSION['user']['id'];
        $reqInfoUser = "SELECT * FROM user WHERE iduser = $myId";
        $resInfoUser = $db->query($reqInfoUser);
        $resultUser = resultAsArray($resInfoUser);
        if ($resultUser) {
            echo json_encode(["success" => true, "infoUser" => $resultUser]);
        }



        break;



    case 'count':

        $reqCountU = "SELECT * FROM user";
        $resCountU = $db->query($reqCountU);
        $resultCountUser = resultAsArray($resCountU);
        $countUser = count($resultCountUser);

        $reqCountP = "SELECT * FROM messages";
        $resCountP = $db->query($reqCountP);
        $resultCountP = resultAsArray($resCountP);
        $countPost = count($resultCountP);

        $reqCountC = "SELECT * FROM conversation";
        $resCountC = $db->query($reqCountC);
        $resultCountC = resultAsArray($resCountC);
        $countConv = count($resultCountC);

        if ($resultCountUser) {
            echo json_encode(["success" => true, "countUser" => $countUser, "countPost" => $countPost, "countConv" => $countConv]);
        }



        break;


    case 'users':

        $myId = $_SESSION['user']['id'];
        $reqUser = $db->query("SELECT * FROM user WHERE iduser NOT IN ($myId) ");
        $resUser = resultAsArray($reqUser);
        if ($resUser) {
            echo json_encode(["userlist" => $resUser]);
        }


        break;

    case 'deleteUser':

        if (!empty($_POST['id'])) {

            $idUser = $_POST['id'];
            // echo $idUser;
            $reqDeleteRelUser = $db->query("DELETE FROM relation_userconv WHERE id_user = $idUser");
            $reqDeletePostUser = $db->query("DELETE FROM messages WHERE user_id = $idUser");
            $reqDeleteMessageUser = $db->query("DELETE FROM messages_conv WHERE id_user = $idUser");
            $reqDeleteUser = $db->query("DELETE FROM user WHERE iduser = $idUser");


            if ($reqDeleteUser) {
                echo json_encode(["success" => true]);
            }
        }

        break;

    case 'deleteConv':

        if (!empty($_POST['id'])) {

            $idConv = $_POST['id'];
            $delMessageConv = $db->query("DELETE FROM messages_conv WHERE id_conv = $idConv");
            $delRelConv = $db->query("DELETE FROM relation_userconv WHERE id_conv = $idConv");
            $delConv = $db->query("DELETE FROM conversation WHERE id = $idConv");
            if ($delConv) {
                echo json_encode(["success" => true]);
            }
        }

        break;

    case 'listConvAdmin':

        $reqConvList = $db->query("SELECT * FROM conversation");
        $resConv = resultAsArray($reqConvList);
        if ($resConv) {
            echo json_encode(["conv" => $resConv]);
        }


        break;
}
