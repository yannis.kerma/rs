<?php
session_start();
require_once('./fonction/db_connect.php');
// require_once('./fonction/checkUserConnect.php');
require_once('../back/fonction/resultArray.php');


switch ($_POST['choice']) {

    case 'convlist':

        $myId = $_SESSION['user']['id']; // Récupération de l'ID utilisateur dans une variable
        $sqlConvList = "SELECT c.*, u.role FROM conversation c JOIN relation_userconv r on c.id = r.id_conv JOIN user u on r.id_user = u.iduser WHERE id_user = $myId "; // Selection des conversations appartenant à l'utilisateur connecté
        $req = $db->query($sqlConvList); // Envoi et recupération de la requete dans une variable
        $result = resultAsArray($req); // Fetch du résultat 

        if ($result) { // Si la requête à fonctionné alors on rentre dans le if
            echo json_encode(['success' => true, 'convlist' => $result]); // Renvoi en JSON le résultat donc la liste des conversations récupéré 
        }

        break;

        // --------------------------------------------------------------------------------

    case 'checkUser':

        if (!isset($_SESSION['user'])) {
            echo json_encode(['success' => false]);
        }

        break;

        // --------------------------------------------------------------------------------


    case 'createconv':

        if (!empty($_POST['convName'])) { // Si convname existe et n'est pas vide alors on rentre dans le if

            $myId = $_SESSION['user']['id']; // Récupération de l'ID utilisateur dans une variable
            $insertConv = "INSERT INTO conversation (nom_conversation) VALUE ('{$_POST['convName']}')"; // Insertion dans la table conversation le nom récupéré
            $reqInsert = $db->query($insertConv); // Envoi de la requête

            if ($reqInsert) { // Si l'insertion est réussi alors on rentre dans le if
                $recupIdConv = "SELECT id FROM conversation WHERE nom_conversation = '{$_POST['convName']}'"; // Récupération de l'Id de la conv par son nom
                $resRecupIdConv = $db->query($recupIdConv); // Envoi de la requete
                $resultIdConv = resultAsArray($resRecupIdConv); // Fetch du résultat

                if ($resultIdConv) { // Si la requête à fonctionné alors on rentre dans le if
                    $idConv = $resultIdConv[0]['id']; // récupération de l'Id de la conv dans une variable
                    $insertRel = $db->query("INSERT INTO relation_userconv (id_user, id_conv) VALUE ($myId,$idConv)"); // Insertion dans la table relation l'id de l'utilisateur et de la conv

                    if ($insertRel) { // Si la requête fonctionne alors rentre dans le if
                        echo json_encode(['success' => true]); // Renvoi du succés en JSON 
                    }
                }
            }
        } else {
            echo json_encode(['success' => false]); // Renvoi success false si une erreur est survenue

        }

        break;

        // --------------------------------------------------------------------------------


    case 'dm_message':

        if (isset($_POST['idconv'])) {

            $myId = $_SESSION['user']['id']; // Récupération de l'ID utilisateur dans une variable
            $convId = $_POST['idconv'];
            $reqSelectMessage = "SELECT messages_conv.*,user.email FROM messages_conv JOIN user on messages_conv.id_user = user.iduser WHERE id_conv = $convId";
            $resSelectMessage = $db->query($reqSelectMessage);
            $resultSelectMessage = resultAsArray($resSelectMessage); // Fetch du résultat

            if ($resultSelectMessage) {
                echo json_encode(["success" => true, "messagelist" => $resultSelectMessage, "idsession" => $myId]);
            } else {
                (["success" => false]);
            }
        }



        break;

        // --------------------------------------------------------------------------------


    case 'envoitext':

        $myId = $_SESSION['user']['id']; // Récupération de l'ID utilisateur dans une variable
        $convId = $_POST['idconv'];
        $message = $_POST['textmessage'];

        $reqEnvoiText = "INSERT INTO messages_conv (content, created_at, id_user, id_conv) VALUES ('$message', NOW(), $myId, $convId)";
        $envoiReq = $db->query($reqEnvoiText);
        if ($envoiReq) {
            echo json_encode(['success' => true]);
        } else {
            echo json_encode(['success' => false]);
        }




        break;


        // --------------------------------------------------------------------------------


    case 'adduserlist':

        $convId = $_POST['idconv'];
        $reqUser = $db->query("SELECT DISTINCT u.* FROM user u LEFT JOIN relation_userconv r
        ON u.iduser = r.id_user
        WHERE r.id_user NOT IN (SELECT id_user FROM relation_userconv r2 WHERE r2.id_conv = $convId) OR r.id_user IS NULL");
        $resultUser = resultAsArray($reqUser);

        if ($resultUser) {
            echo json_encode((["success" => true, "user" => $resultUser]));
        }

        break;



        // --------------------------------------------------------------------------------


    case 'userlist':

        $convId = $_POST['idconv'];
        $reqUser = $db->query("SELECT * from USER u JOIN relation_userconv r ON u.iduser = r.id_user WHERE r.id_conv = $convId");
        $resultUser = resultAsArray($reqUser);

        if ($resultUser) {
            echo json_encode((["success" => true, "user" => $resultUser]));
        }

        break;


        // --------------------------------------------------------------------------------


    case 'adduser':

        $convId = $_POST['idconv'];
        $idnewuser = $_POST['userid'];

        $insertNewUser = $db->query("INSERT INTO relation_userconv (id_user, id_conv) VALUES ($idnewuser, $convId)");
        if ($insertNewUser) {
            echo json_encode(["success" => true]);
        }

        break;


        // --------------------------------------------------------------------------------


    case 'infoUser':


        $id = $_SESSION['user']['id'];
        $reqInfoUser = $db->query("SELECT * FROM user WHERE iduser = $id");
        $user = resultAsArray($reqInfoUser);
        if ($user) {
            echo json_encode(["success" => true, "user" => $user]);
        }



        break;
}
